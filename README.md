﻿# diregaming.localization

This project is to get weather of the city and return the weather based on selected language.
Weather information is from https://openweathermap.org/

### Prerequisites

.NET core 3.1

## Running the application

1. Run the application using .net cli:

```
dotnet run
```

2. Open browser, go to http://localhost:5000/api/OpenWeather/{city}?culture={lang}
change {city} to city name, and {lang} to en-US, de-CH, fr-CH or it-CH 

example: http://localhost:5000/api/OpenWeather/Singapore?culture=de-CH

## Authors

* **Yayang** - [ryuki](https://gitlab.com/ryuki)

## License

This project is licensed under the MIT License
