﻿using AutoMapper;
using diregaming.localization.Dtos.OpenWeather;
using diregaming.localization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace diregaming.localization
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<OpenWeather, GetOpenWeatherDto>();
        }
    }
}
