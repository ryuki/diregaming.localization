﻿using diregaming.localization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace diregaming.localization.Dtos.OpenWeather
{
    public class GetOpenWeatherDto
    {
        public Coord coord { get; set; }
        public List<Weather> weather { get; set; }
        public Main main { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int timezone { get; set; }
        public int cod { get; set; }
    }
}
