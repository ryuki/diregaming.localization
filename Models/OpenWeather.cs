﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace diregaming.localization.Models
{
    public class OpenWeather
    {
        public Coord coord { get; set; }
        public List<Weather> weather { get; set; }
        public Main main { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int timezone { get; set; }
        public int cod { get; set; }
    }

    public class Coord
    {
        public decimal lon { get; set; }
        public decimal lat { get; set; }
    }

    public class Main
    {
        public decimal temp { get; set; }
        public decimal feels_like { get; set; }
        public decimal temp_min { get; set; }
        public decimal temp_max { get; set; }
        public decimal pressure { get; set; }
        public decimal humidity { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }

    }
}
