using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Localization.SqlLocalizer.DbStringLocalizer;
using Localization.SqlLocalizer.IntegrationTests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace diregaming.localization.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AboutController : ControllerBase
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IStringLocalizer<AboutController> _aboutLocalizerizer;
        private readonly IStringExtendedLocalizerFactory _stringExtendedLocalizerFactory;
        private readonly LocalizationModelContext _context;


        public AboutController(IStringLocalizer<SharedResource> localizer, IStringLocalizer<AboutController> aboutLocalizerizer, IStringExtendedLocalizerFactory stringExtendedLocalizerFactory, LocalizationModelContext context)
        {
            _localizer = localizer;
            _aboutLocalizerizer = aboutLocalizerizer;
            _stringExtendedLocalizerFactory = stringExtendedLocalizerFactory;
            _context = context;
        }

        [HttpGet()]
        public string Get()
        {
            string result = _aboutLocalizerizer["AboutTitle"];
            return result;
        }
    }
}