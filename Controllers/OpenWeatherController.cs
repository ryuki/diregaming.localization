﻿using diregaming.localization.Services.OpenWeatherService;
using Localization.SqlLocalizer.DbStringLocalizer;
using Localization.SqlLocalizer.IntegrationTests;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace diregaming.localization.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OpenWeatherController : ControllerBase
    {
        private readonly IStringLocalizer<SharedResource> _localizer;
        private readonly IStringLocalizer<OpenWeatherController> _openWeatherLocalizer;
        private readonly IStringExtendedLocalizerFactory _stringExtendedLocalizerFactory;
        private readonly LocalizationModelContext _context;
        private readonly IOpenWeatherService _openWeatherService;
        public OpenWeatherController(IOpenWeatherService openWeatherService, IStringLocalizer<SharedResource> localizer, IStringLocalizer<OpenWeatherController> openWeatherLocalizer, IStringExtendedLocalizerFactory stringExtendedLocalizerFactory, LocalizationModelContext context)
        {
            this._localizer = localizer;
            this._openWeatherLocalizer = openWeatherLocalizer;
            this._stringExtendedLocalizerFactory = stringExtendedLocalizerFactory;
            this._context = context;
            this._openWeatherService = openWeatherService;

        }

        [HttpGet("{city}")]
        public async Task<IActionResult> GetOpenWeatherByCity(string city)
        {
            string language = _localizer["Lang"];
            return Ok(await _openWeatherService.GetOpenWeatherByCity(city, language));
        }
    }
}
