﻿using AutoMapper;
using diregaming.localization.Dtos.OpenWeather;
using diregaming.localization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.Threading;

namespace diregaming.localization.Services.OpenWeatherService
{
    public class OpenWeatherService : IOpenWeatherService
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public OpenWeatherService(IMapper mapper, IConfiguration configuration)
        {
            this._mapper = mapper;
            this._configuration = configuration;
        }
        public async Task<ServiceResponse<GetOpenWeatherDto>> GetOpenWeatherByCity(string city, string lang)
        {
            ServiceResponse<GetOpenWeatherDto> serviceResponse = new ServiceResponse<GetOpenWeatherDto>();
            string appId = _configuration.GetValue<string>("AppId");
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(string.Format("https://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}&lang={2}", city, appId, lang));

                if (response.IsSuccessStatusCode)
                {
                    Console.Write("Success");
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    serviceResponse.Data = _mapper.Map<GetOpenWeatherDto>(JsonConvert.DeserializeObject<OpenWeather>(apiResponse));
                    serviceResponse.Message = response.RequestMessage.RequestUri.ToString();
                }
                else
                {
                    Console.Write("Failure");
                    serviceResponse.Success = false;
                    serviceResponse.Message = response.StatusCode.ToString();
                }
            }
            return serviceResponse;
        }
    }
}
