﻿using diregaming.localization.Dtos.OpenWeather;
using diregaming.localization.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace diregaming.localization.Services.OpenWeatherService
{
    public interface IOpenWeatherService
    {
        Task<ServiceResponse<GetOpenWeatherDto>> GetOpenWeatherByCity(string city, string lang);
    }
}
